import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';
import { Users } from './users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private URLl = "https://jsonplaceholder.typicode.com/users/";
  getusers():Observable<Users> {
    return this.http.get<Users>(this.URLl)
         
  }
  
  constructor(private http: HttpClient) { }
}
