import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
   authors: any =  [{id:1, author:'Lewis Carrol'},{id:2, author:'Leo Tolstoy'}, {id:3, author:'Thomas Mann'}]
 /*  
  getAuthors(){
    setInterval(() => {return this.authors}, 4000);
  }*/
 
  
  addAuthors(authornew:string){
    this.authors.push({author: authornew});
  };

  getAuthors(): any {
    const authorsObservable = new Observable(observer => {
           setInterval(() => 
               observer.next(this.authors)
           , 4000);
    });

    return authorsObservable;
}
 
  constructor() { }
}
