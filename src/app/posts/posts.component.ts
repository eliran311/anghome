import { Component, OnInit } from '@angular/core';
import { Posts } from './../posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
// import { Users } from '../users';
import { Router, ActivatedRoute } from '@angular/router';
//import { UsersService} from './../users.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  post$: Observable<any>;
  // user$ : Users [] = [];
  body;
  title;
  author;
  id;
  ans:string;
  
 //posts:Posts[];
 /*deletePost(id){
  
  for (let i = 0; i < this.post$.length; i++) {
      if (this.post$[i].id==id)
       {
         this.postsservice.deleteposts(id);
         this.post$.splice(i,1);
}
  }
}*/
  
  constructor(private postsservice:PostsService, private router:Router,
    private route: ActivatedRoute) { }
    deletePost(id:string){
      this.postsservice.deleteposts(id);
    }
 /* myFunc(){
    for (let index = 0; index < this.post$.length; index++) {
      for (let i = 0; i < this.user$.length; i++) {
        if (this.post$[index].userId==this.user$[i].id) {
          this.title = this.post$[index].title;
          this.body = this.post$[index].body;
          this.author = this.user$[i].name;
          this.postsservice.addPost(this.body,this.author,this.title);
          
          
        }
        
        
      }
      
    }
    this.ans ="The data retention was successful"
  }
   */
  ngOnInit() {
    //geting posts from db
     this.post$ = this.postsservice.getPosts();
     console.log(this.post$);
 /*  this.postsservice.getposts()
    .subscribe(posts => this.post$ = posts); 
    this.postsservice.getusers()
    .subscribe(users => this.user$ = users);*/
  
} 

          
  }


