import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Posts } from './posts';
import { Users } from './users';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  private URL1 = "https://jsonplaceholder.typicode.com/users/";
 
 /* getposts() {
    return this.http.get<Posts[]>(this.URL)
         
  }*/
  /*
  getPosts() {
    return this.db.collection('posts').valueChanges();
         
  }*/
 /* getusers() {
    return this.http.get<Users[]>(this.URL1)
         
  }*/
   getPosts():Observable<any[]>{
    return this.db.collection('posts').valueChanges({idField:'id'});
  }
  getPost(id:string):Observable<any>{
        return this.db.doc(`posts/${id}`).get();
      }
  addPost(body:string, author:string,name:string) {
    const post = {body:body, author:author, name:name};
  this.db.collection('posts').add({post});
  
  }
  deleteposts(id:string){
  return  this.db.doc(`posts/${id}`).delete();
  }
  
    updatePost(id:string,body:string,author:string,title:string){
      this.db.doc(`posts/${id}`).update(
        {
          title:title,
          author:author,
          body:body
        }
      )
    }
  constructor(private http: HttpClient,private db:AngularFirestore) { }
 
}
