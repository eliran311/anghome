import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from '../posts.service';
import { Posts } from '../posts';
@Component({
  selector: 'app-addposts',
  templateUrl: './addposts.component.html',
  styleUrls: ['./addposts.component.css']
})
export class AddpostsComponent implements OnInit {

  constructor(private router: Router, private postsservice:PostsService, private route:ActivatedRoute) { }
  body:string;
  title:string;
  id:string;
  author:string;
  isEdit:boolean = false;
buttonText:string = "Add Post";
  onSubmit(){ 
if(this.isEdit){
this.postsservice.updatePost(this.id, this.title, this.body,this.author)
}else{

    this.postsservice.addPost(this.title,this.author,this.body)
}
    this.router.navigate(['/posts']);
  }  
 

  ngOnInit() {
this.id = this.route.snapshot.params.id;
console.log(this.id);
if(this.id){
this.isEdit = true;
this.buttonText = "Update";
this.postsservice.getPost(this.id).subscribe(
  post => {
    console.log(post.data().author)
    console.log(post.data().title)
    this.author = post.data().author;
    this.title = post.data().title;
    this.body = post.data().body;
  }
)
}
  }

}

