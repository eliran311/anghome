import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorsService} from './../authors.service';
@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

 // typesOfAuthors: object[] = [{id:1,author:'Lewis Carrol'}, {id:2,author:'CLeo Tolstoy'}, {id:3,author:'Thomas Mann'}];
 
 authors$:Observable<any>; 
 authornew:string;//לא חייב אותו שם
 constructor( private authorservice:AuthorsService) { }
  add(){
    this.authorservice.addAuthors(this.authornew);//קורא לפונקציה בסרביס
  }  
  ngOnInit() {
   this.authors$ = this.authorservice.getAuthors();
   
  }

}
