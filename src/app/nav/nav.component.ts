import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})

export class NavComponent {
title:String;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router,location: Location, public authService:AuthService) {
  router.events.subscribe((val) => {
    if(location.path() == '/books'){
      this.title = "books";
    }
    else  if(location.path() == '/authors'){
        this.title = "authors";
      }
      else  if(location.path() == '/posts'){
        this.title = "posts";
      }
      else  if(location.path() == '/signup'){
        this.title = "Sign up";
      }
      else  if(location.path() == '/login'){
        this.title = "Login";
      }
  });
}
}