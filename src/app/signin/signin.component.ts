import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
email:string;
password:string;
 
  constructor(private Authservice:AuthService, private router: Router,private route:ActivatedRoute) { }

  ngOnInit() {

  }
onSubmit(){
  this.Authservice.signin(this.email,this.password);
  this.router.navigate(['/books']);

}
}
