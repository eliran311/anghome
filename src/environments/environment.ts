// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyBMJQgSCuRKhTTnG3YcPxwN10nWHh0ixa0",
    authDomain: "anghome-d5bcb.firebaseapp.com",
    databaseURL: "https://anghome-d5bcb.firebaseio.com",
    projectId: "anghome-d5bcb",
    storageBucket: "anghome-d5bcb.appspot.com",
    messagingSenderId: "455540963716",
    appId: "1:455540963716:web:a7220bb9a280d0f5fe45d1",
    measurementId: "G-7CT07Y4SCZ"
  }

};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
